﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace EmailTask.Models
{
    public class SubmitInfo
    {
        [Required]
        [EmailAddress]
        [JsonProperty("email")]
        public string Email { get; set; }
    }
}