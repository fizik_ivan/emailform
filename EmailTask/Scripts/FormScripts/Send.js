const form = document.getElementById("form_send");

var modal = document.getElementById("SuccessModal");
var modalFail = document.getElementById("FailModal");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function () {
    modal.style.display = "none";
    modalFail.style.display = "none"
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function (event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
    if (event.target == modalFail) {
        modalFail.style.display = "none"
    }
}

//When form is submitted we intercept
form.addEventListener('submit', SubmitForm);

//When Send button is clicked make POST request to controller and stop the default submission.
function SubmitForm(event) {
    event.preventDefault();
    $.ajax({
        async: true,
        data: $('#form_send').serialize(),
        type: "POST",
        url:'/Home/Send',
        success: function () {
            modal.style.display = "block";  //Show success modal
        },
        error: function () {
            modalFail.style.display = "block";  //Show fail modal
        }
    });
};