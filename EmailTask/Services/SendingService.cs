﻿using EmailTask.Models;
using EmailTask.Services.Contracts;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;


namespace EmailTask.Services
{
    public class SendingService : IService
    {
        private readonly HttpClient client;

        /// <summary>
        /// Creates instance of the service and sets the HttpClient to be used.
        /// </summary>
        /// <param name="httpClient">HttpClient used for sending requests to outsude service.</param>
        public SendingService(HttpClient httpClient)
        {
            this.client = httpClient ?? throw new ArgumentNullException("Client was not provided");
        }

        /// <summary>
        /// Sends requests to outside service as well as catching any exception that may occur.
        /// </summary>
        /// <param name="info">Information being send via the request.</param>
        /// <returns>Bool</returns>
        public async Task<bool> SendAsync(SubmitInfo info)
        {
            try
            {
                var response = new HttpResponseMessage();
                //Task says until succesfull but implementing limit on failed attempts seem more reasonable.
                for (int i = 0; i < 10; i++)
                {
                    response = await client.SendAsync(CreateRequest(info));
                    if (response.IsSuccessStatusCode)
                    {
                        return true;
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
            return false;
        }

        /// <summary>
        /// Build the request content.
        /// </summary>
        /// <param name="info">Information being send via the request.</param>
        /// <returns>build HttpRequestMessage</returns>
        private HttpRequestMessage CreateRequest(SubmitInfo info)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, "https://us-central1-randomfails.cloudfunctions.net/submitEmail");
            request.Content = new StringContent(JsonConvert.SerializeObject(info), Encoding.UTF8, "application/json");
            return request;
        }
    }
}