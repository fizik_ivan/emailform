﻿using EmailTask.Models;
using System.Threading.Tasks;

namespace EmailTask.Services.Contracts
{
    public interface IService
    {
        Task<bool> SendAsync(SubmitInfo info);
    }
}
