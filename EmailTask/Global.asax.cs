﻿using Autofac;
using Autofac.Integration.Mvc;
using EmailTask.Controllers;
using EmailTask.Services;
using EmailTask.Services.Contracts;
using System.Net.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace EmailTask
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<HomeController>().InstancePerRequest();
            builder.RegisterType<SendingService>().As<IService>().InstancePerRequest();
            builder.RegisterType<HttpClient>().InstancePerRequest();

            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}
