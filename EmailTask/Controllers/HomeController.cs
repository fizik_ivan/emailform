﻿using EmailTask.Models;
using EmailTask.Services.Contracts;
using System;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace EmailTask.Controllers
{
    public class HomeController : Controller
    {
        private readonly IService service;

        /// <summary>
        /// Creates instance of Home controller and set used service.
        /// </summary>
        /// <param name="service">Used service</param>
        public HomeController(IService service)
        {
            this.service = service ?? throw new ArgumentNullException("Service was not provided");
        }

        /// <summary>
        /// Initial action displaying default view.
        /// </summary>
        /// <returns>Index View</returns>
        public ActionResult Index()
        {
            return View("Index");
        }

        /// <summary>
        /// Post action using incoming information to call working service.
        /// </summary>
        /// <param name="info">Object coming from form of Index View</param>
        /// <returns>Returns confirmation if the service was succesfull or not (status code)</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Send([Bind(Include = "Email")] SubmitInfo info)
        {

            if (ModelState.IsValid && info != null && new EmailAddressAttribute().IsValid(info.Email))
            {
                if (await service.SendAsync(info))
                {
                    return new HttpStatusCodeResult(200);
                }
            }
            return new HttpStatusCodeResult(500);
        }
    }
}