# EmailForm

### General Points of Interest
    1 Model, 1 Controller, 1 View.
    1 Service handling the business logic.
    Written for .NET Framework (4.7.2)
    Autofac used as DI container.
    Moq used for testing.
    (Those NuGet packages would be needed for project)

### Model
    SubmitInfo - The class has only 1 string property named "Email", it has attributes flagging it as Required, EmailAddress and also has it's Json name specified as "email". The model is placed in folder "Models".

### Controller
    HomeController - Inherits Controller. Has a private readonly field of type IService. Has 3 methods. Placed in folder "Controllers".
        Constructor - Public. Set the service field or throw exception if none provided (DI container handles provision).
        Index() - Public. Initial method called at the start of the application. Returns the Index View.
        Send(SubmitInfo) - Public, require parameter of type SubmitInfo.
            Validates the parameter.
            If parameter is valid calls asynchronously the service with that parameter.
            If the return of service is true returns HttpStatusCodeResult(200).
            In all other cases returns HttpStatusCodeResult(500).

### View
    Index - Contains form with 1 field for E-mail, button for sending, as well as 2 hidden sections for modals. At the end there is a section for scripts. Placed in folder "Views".
        The scripts used are in folder "Scripts".
        The css used are in folder "Content".

        Site.css - has added settings for use with the view modals.
        Send.js - placed in "Scripts/FormScripts" has script manipulating the modal displaying, as well as operates the "Send" button of the form. The button sends ajax request to controller.

### Service
    SendingService - Implements IService. Has a private readonly field of type HttpClient.
    Has 3 methods. Placed in folder "Services".
        Constructor - Public. Set the client field or throw exception if none provided (DI container handles provision).
        SendAsync(SubmitInfo) - Public, require parameter of type SubmitInfo. 
            In a try/catch block declares a response variable and after that enters a for cycle with 10 steps, where a method building the request with given parameter to outside service is called and send asynchronously with the client. The result is recorded in the response variable.
            If the response is success code the method returns true.
            In all other cases method return false.
        CreateRequest(SubmitInfo) - Private, require parameter of type SubmitInfo.
            New request message is created with URL to outside service.
            Request content is filled according to parameter.
            The result is returned.