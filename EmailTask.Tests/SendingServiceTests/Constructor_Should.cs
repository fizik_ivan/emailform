﻿using EmailTask.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace EmailTask.Tests.SendingServiceTests
{
    [TestClass]
    public class Constructor_Should
    {
        [TestMethod]
        public void Constructor_Should_ThrowArgumentNullExceptionIfNoClientProvided()
        {
            Assert.ThrowsException<ArgumentNullException>(() => new SendingService(null), "Client was not provided");
        }
    }
}
