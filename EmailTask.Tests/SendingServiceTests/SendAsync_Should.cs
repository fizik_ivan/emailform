﻿using EmailTask.Models;
using EmailTask.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Moq.Protected;
using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace EmailTask.Tests.SendingServiceTests
{
    [TestClass]
    public class SendAsync_Should
    {
        [TestMethod]
        public async Task SendAsync_Should_ReturnTrueIfSuccesfull()
        {
            var handlerMock = new Mock<HttpMessageHandler>(MockBehavior.Strict);
            handlerMock
               .Protected()
               .Setup<Task<HttpResponseMessage>>(
                  "SendAsync",
                  ItExpr.IsAny<HttpRequestMessage>(),
                  ItExpr.IsAny<CancellationToken>()
               )
               .ReturnsAsync(new HttpResponseMessage()
               {
                   StatusCode = HttpStatusCode.OK,
                   Content = new StringContent(string.Empty),
               })
               .Verifiable();
            var httpClient = new HttpClient(handlerMock.Object)
            {
                BaseAddress = new Uri("http://sometest.com/"),
            };
            var sut = new SendingService(httpClient);
            var model = new SubmitInfo() { Email = "Valid@Mail.Address" };


            var result = await sut.SendAsync(model);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public async Task SendAsync_Should_ReturnFalseIfAllAttemptsFail()
        {
            var handlerMock = new Mock<HttpMessageHandler>(MockBehavior.Strict);
            handlerMock
               .Protected()
               .Setup<Task<HttpResponseMessage>>(
                  "SendAsync",
                  ItExpr.IsAny<HttpRequestMessage>(),
                  ItExpr.IsAny<CancellationToken>()
               )
               .ReturnsAsync(new HttpResponseMessage()
               {
                   StatusCode = HttpStatusCode.BadRequest,
                   Content = new StringContent(string.Empty),
               })
               .Verifiable();
            var httpClient = new HttpClient(handlerMock.Object)
            {
                BaseAddress = new Uri("http://sometest.com/"),
            };
            var sut = new SendingService(httpClient);
            var model = new SubmitInfo() { Email = "Valid@Mail.Address" };


            var result = await sut.SendAsync(model);

            Assert.IsFalse(result);
        }

        [TestMethod]
        public async Task SendAsync_Should_ReturnFalseIfExceptionHappen()
        {
            var handlerMock = new Mock<HttpMessageHandler>(MockBehavior.Strict);
            handlerMock
               .Protected()
               .Setup<Task<HttpResponseMessage>>(
                  "SendAsync",
                  ItExpr.IsAny<HttpRequestMessage>(),
                  ItExpr.IsAny<CancellationToken>()
               )
               .ReturnsAsync(() => throw new Exception())
               .Verifiable();
            var httpClient = new HttpClient(handlerMock.Object)
            {
                BaseAddress = new Uri("http://sometest.com/"),
            };
            var sut = new SendingService(httpClient);
            var model = new SubmitInfo() { Email = "Valid@Mail.Address" };


            var result = await sut.SendAsync(model);

            Assert.IsFalse(result);
        }
    }
}
