using EmailTask.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace EmailTask.Tests.HomeControllerTests
{
    [TestClass]
    public class Constructor_Should
    {
        [TestMethod]
        public void Constructor_Should_ThrowArgumentNullExceptionIfNoServiceProvided()
        {
            Assert.ThrowsException<ArgumentNullException>(() => new HomeController(null), "Service was not provided");
        }
    }
}
