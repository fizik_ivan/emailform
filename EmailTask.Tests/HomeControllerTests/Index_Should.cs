﻿using EmailTask.Controllers;
using EmailTask.Services.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Web.Mvc;

namespace EmailTask.Tests.HomeControllerTests
{
    [TestClass]
    public class Index_Should
    {
        [TestMethod]
        public void Index_Should_ReturnIndexView()
        {
            var service = new Mock<IService>();
            var sut = new HomeController(service.Object);
            var result = sut.Index() as ViewResult;
            Assert.AreEqual(result.ViewName, "Index");
        }
    }
}
