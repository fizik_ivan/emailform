﻿using EmailTask.Controllers;
using EmailTask.Models;
using EmailTask.Services.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace EmailTask.Tests.HomeControllerTests
{
    [TestClass]
    public class Send_Should
    {
        [TestMethod]
        public async Task Send_Should_ReturnStatus500IfEmailNotValid()
        {
            var service = new Mock<IService>();
            var sut = new HomeController(service.Object);
            var model = new SubmitInfo() { Email = "NotAValidMail" };
            var result = await sut.Send(model);
            Assert.IsInstanceOfType(result, typeof(HttpStatusCodeResult));
            var status = result as HttpStatusCodeResult;
            Assert.AreEqual(status.StatusCode, 500);
        }

        [TestMethod]
        public async Task Send_Should_ReturnStatus500IfModelNull()
        {
            var service = new Mock<IService>();
            var sut = new HomeController(service.Object);
            var result = await sut.Send(null);
            Assert.IsInstanceOfType(result, typeof(HttpStatusCodeResult));
            var status = result as HttpStatusCodeResult;
            Assert.AreEqual(status.StatusCode, 500);
        }

        [TestMethod]
        public async Task Send_Should_ReturnStatus500IfSendingServiceReturnFalse()
        {
            var service = new Mock<IService>();
            service.Setup(x => x.SendAsync(It.IsAny<SubmitInfo>())).ReturnsAsync(false);
            var sut = new HomeController(service.Object);
            var model = new SubmitInfo() { Email = "Valid@Mail.Address" };
            var result = await sut.Send(model);
            Assert.IsInstanceOfType(result, typeof(HttpStatusCodeResult));
            var status = result as HttpStatusCodeResult;
            Assert.AreEqual(status.StatusCode, 500);
        }

        [TestMethod]
        public async Task Send_Should_ReturnStatus200IfSendingServiceReturnTrue()
        {
            var service = new Mock<IService>();
            service.Setup(x => x.SendAsync(It.IsAny<SubmitInfo>())).ReturnsAsync(true);
            var sut = new HomeController(service.Object);
            var model = new SubmitInfo() { Email = "Valid@Mail.Address" };
            var result = await sut.Send(model);
            Assert.IsInstanceOfType(result, typeof(HttpStatusCodeResult));
            var status = result as HttpStatusCodeResult;
            Assert.AreEqual(status.StatusCode, 200);
        }
    }
}
